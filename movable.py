import math

class Movable:
    def __init__(self, x, y, dx, dy, world_width, world_height):
        self.mX = x
        self.mY = y
        self.mDX = dx
        self.mDY = dy
        self.mWorldWidth = world_width
        self.mWorldHeight = world_height
        self.mActive = True
        return

    def getX (self):
        return self.mX
    def getY (self):
        return self.mY
    def getDX (self):
        return self.mDX
    def getDY (self):
        return self.mDY
    def getWorldWidth (self):
        return self.mWorldWidth
    def getWorldHeight(self):
        return self.mWorldHeight
    def getActive(self):
        return self.mActive

    def setActive(self, active):
        self.mActive = active
        return

    def move(self, dt):
        self.mX += self.mDX * dt
        self.mY += self.mDY * dt
        if self.mX < 0:
            self.mX += self.mWorldWidth
        elif self.mX >= self.mWorldWidth:
            self.mX -= self.mWorldWidth
        if self.mY < 0:
            self.mY += self.mWorldHeight
        elif self.mY >= self.mWorldHeight:
            self.mY -= self.mWorldHeight
        return

    def hits(self, other):
        boo = True
        together_radii = self.getRadius() + other.getRadius()
        distance = math.sqrt((self.getX() -other.getX()) **2  + (self.getY()- other.getY()) **2 )
        if together_radii > distance:
            boo = False
            self.setActive(boo)
            other.setActive(boo)
        return boo

    def getRadius(self):
        raise NotImplementedError
        return

    def accelerate(self, delta_velocity):
        raise NotImplementedError
        return
    def evolve(self, dt):
        raise NotImplementedError
        return
    def draw(self, surface):
        raise NotImplementedError
        return
