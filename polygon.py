import rotatable
import math
import pygame

class Polygon(rotatable.Rotatable):
    def __init__(self, x, y, dx, dy, rotation, world_width, world_height):
        rotatable.Rotatable.__init__(self, x, y, dx, dy, rotation, world_width, world_height)
        self.mOriginalPolygon = []
        self.mColor = (255,255,255)
        return

    def getRadius(self):
        if len(self.mOriginalPolygon) == 0:
            total = 0
        else:
            total = 0
            for i, j in self.mOriginalPolygon:
                radius = math.sqrt((i **2) + (j **2))
                total += radius
            total /= len(self.mOriginalPolygon)
        return total
            
    def getPolygon(self):
        return self.mOriginalPolygon
    def getColor(self):
        return self.mColor

    def setPolygon(self, some_list):
        self.mOriginalPolygon = []
        for i,j in some_list:
            self.mOriginalPolygon.append((i,j))
        return
    def setColor(self, color):
        self.mColor = color
        return

    def draw(self, surface):
        points = self.rotateAndTranslatePointList(self.mOriginalPolygon)
        pygame.draw.polygon( surface, self.mColor, points, 1)
        return




