import movable
import math

class Rotatable(movable.Movable):
    def __init__(self, x, y, dx, dy, rotation, world_width, world_height):
        movable.Movable.__init__(self, x, y, dx, dy, world_width, world_height)
        self.mRotation = rotation
        return

    def getRotation(self):
        return self.mRotation

    def rotate(self, delta_rotation):
        self.mRotation += delta_rotation
        if self.mRotation >= 360:
            self.mRotation -= 360
        elif self.mRotation < 0:
            self.mRotation += 360
        return

    def splitDeltaVIntoXAndY (self, rotation, delta_velocity):
        angle = math.radians(rotation)
        horizontal = math.cos(angle)
        vertical = math.sin(angle)
        horizontal *= delta_velocity
        vertical *= delta_velocity
        return horizontal, vertical

    def accelerate(self, delta_velocity):
        horizontal, vertical = self.splitDeltaVIntoXAndY(self.mRotation, delta_velocity)
        self.mDX += horizontal
        self.mDY += vertical
        return

    def rotatePoint(self, x, y):
        angle_now = math.atan2(y,x)
        rotation = math.radians(self.mRotation)
        angle_needed = angle_now + rotation
        distance = math.sqrt(x**2 + y**2)
        horizontal = math.cos(angle_needed)
        vertical = math.sin(angle_needed)
        x = horizontal * distance
        y = vertical * distance
        return x,y

    def translatePoint(self, x, y):
        x += self.mX
        y += self.mY
        return x,y

    def rotateAndTranslatePoint(self, x, y):
        x, y = self.rotatePoint(x,y)
        x, y = self.translatePoint(x,y)
        return x,y

    def rotateAndTranslatePointList(self, point_list):
        new_list = []
        for i,j in point_list:
            new_list.append(self.rotateAndTranslatePoint(i,j))
        return new_list
    

