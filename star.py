import circle
import random

class Star(circle.Circle):
    def __init__(self, x, y, world_width, world_height):
        circle.Circle.__init__(self, x, y, 0, 0, 0, 2, world_width, world_height)
        self.mBrightness = random.randrange(0,256)
        return

    def getBrightness(self):
        return self.mBrightness

    def setBrightness(self, brightness):
        if brightness >= 0 and brightness <= 255:
            self.mBrightness = brightness
            self.setColor((brightness, brightness, brightness))
        return

    def evolve(self, dt):
        chance = random.randint(1,2)
        if chance == 1:
            self.setBrightness(self.mBrightness + 10)
            if self.mBrightness > 255:
                self.setBrightness( self.mBrightness - 255)
        elif chance == 2:
            self.setBrightness(self.mBrightness - 10)
            if self.mBrightness < 0:
                self.setBrightness( self.mBrightness + 255)
        return
