import polygon
import random
import math

class Rock (polygon.Polygon):
    def __init__(self, x, y, world_width, world_height):
        rotation = random.uniform(0.0, 359.9)
        polygon.Polygon.__init__(self, x, y, 0, 0, rotation, world_width, world_height)
        self.mSpinRate = random.uniform(-90, 90)
        self.accelerate(random.uniform(10,20))
        poly = self.createRandomPolygon(20, 10)
        polygon.Polygon.setPolygon(self, poly)
        return

    def getSpinRate(self):
        return self.mSpinRate
    def setSpinRate(self, spin_rate):
        if spin_rate <= 90 and spin_rate >= -90:
            self.mSpinRate = spin_rate

    def createRandomPolygon(self, radius, number_of_points):
        list_of_points = []
        list_of_distances = []
        for i in range(number_of_points):
            angle_in_degrees = i*360 / number_of_points
            angle_in_radians = math.radians(angle_in_degrees)
            distance = random.uniform(.7*radius, 1.3*radius)
            x = math.cos(angle_in_radians)*distance
            y = math.sin(angle_in_radians)*distance
            list_of_points.append((x,y))
        return list_of_points

    def evolve(self, dt):
        self.rotate(self.mSpinRate*dt)
        self.move(dt)
        return
            
            
        
                        


