import polygon
import bullet

class Ship(polygon.Polygon):
    def __init__ (self, x, y, world_width, world_height):
        polygon.Polygon.__init__(self, x, y, 0, 0, 0, world_width, world_height)
        self.setPolygon([(-10, 10), (-10, -10), (12, 0)])
        return

    def evolve(self, dt):
        self.move(dt)
        return

    def fire(self):
        points = self.getPolygon()
        point_x, point_y = points[2]
        x, y = self.rotateAndTranslatePoint(point_x, point_y)
        bull = bullet.Bullet(x, y, self.mDX, self.mDY, self.mRotation, self.mWorldWidth, self.mWorldHeight)
        return bull
        




