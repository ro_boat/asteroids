import ship
import rock
import random
import math
import pygame
import star
import bullet

class Asteroids:
    def __init__(self, world_width, world_height):
        self.mWorldWidth = world_width
        self.mWorldHeight = world_height
        self.mBlack = (0,0,0)
        x = self.mWorldWidth/2
        y = self.mWorldHeight/2
        self.mShip = ship.Ship(x, y, self.mWorldWidth, self.mWorldHeight)
        self.mRocks = []
        for i in range(10):
            x = random.randrange(0, self.mWorldWidth)
            y = random.randrange(0, self.mWorldHeight)
            self.mRocks.append( rock.Rock(x, y, self.mWorldWidth, self.mWorldHeight))
        self.mStars = []
        for i in range(20):
            x = random.randrange(0, self.mWorldWidth)
            y = random.randrange(0, self.mWorldHeight)
            self.mStars.append( star.Star( x, y, self.mWorldWidth, self.mWorldHeight))
        self.mBullets = []
        self.mObjects = []
        self.mObjects.append(self.mShip)
        for i in range(len(self.mRocks)):
            self.mObjects.append(self.mRocks[i])
        for i in range(len(self.mStars)):
            self.mObjects.append(self.mStars[i])
        return

    def getWorldWidth(self):
        return self.mWorldWidth
    def getWorldHeight(self):
        return self.mWorldHeight
    def getShip(self):
        return self.mShip
    def getRocks(self):
        return self.mRocks
    def getObjects(self):
        return self.mObjects
    def getStars(self):
        return self.mStars
    def getBullets(self):
        return self.mBullets

    def turnShipLeft(self, delta_rotation):
        self.mShip.rotate(-delta_rotation)
        return

    def turnShipRight(self, delta_rotation):
        self.mShip.rotate(delta_rotation)
        return

    def accelerateShip(self, delta_velocity):
        self.mShip.accelerate(delta_velocity)
        return

    def evolve(self, dt):
        self.evolveAllObjects(dt)
        self.collideShipAndBullets()
        self.collideShipAndRocks()
        self.collideRocksAndBullets()
        self.removeInactiveObjects()
        return

    def evolveAllObjects(self, dt):
        for i in self.mObjects:
            i.evolve(dt)
        return

    def collideShipAndBullets(self):
        for i in self.mBullets:
            self.mShip.hits(i)
        return
    def collideShipAndRocks(self):
        for i in self.mRocks:
            self.mShip.hits(i)
        return
    def collideRocksAndBullets(self):
        for i in self.mRocks:
            for j in self.mBullets:
                i.hits(j)
        return

    def removeInactiveObjects(self):
        new_objects = []
        new_bullets = []
        new_rocks = []
        for b in self.mBullets:
            if b.getActive():
                new_objects.append(b)
                new_bullets.append(b)
        for r in self.mRocks:
            if r.getActive():
                new_objects.append(r)
                new_rocks.append(r)
        if self.mShip.getActive():
            new_objects.append(self.mShip)
        self.mObjects = new_objects
        for i in self.mStars:
            self.mObjects.append(i)
        self.mBullets = new_bullets
        self.mRocks = new_rocks
        return


    def fire(self):
        if self.mShip.getActive():
            if len(self.mBullets) <= 2:
                self.mBullets.append( self.mShip.fire())
                self.mObjects.append( self.mBullets[-1] )
        return

    def draw(self, surface):
        rect = pygame.Rect( int ( 0 ), int ( 0 ), int ( self.mWorldWidth ), int ( self.mWorldHeight ) )
        pygame.draw.rect( surface, self.mBlack, rect, 0 )
        for i in self.mObjects:
            i.draw(surface)
        return
